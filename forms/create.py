from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired

class CreateForm(FlaskForm):
    id = IntegerField("Id: ", validators=[DataRequired()])
    content = StringField("Content: ", validators=[DataRequired()])
