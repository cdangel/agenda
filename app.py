from flask import Flask
from blueprints.agenda import agenda

app = Flask(__name__)
app.secret_key = "qwerty12345"

app.register_blueprint(agenda, url_prefix='/agenda/api')