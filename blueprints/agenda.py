import json

from flask import Blueprint, url_for, redirect, render_template
from forms.create import CreateForm

dataAgenda = json.loads(
	json.dumps([
		{
			'id': 1,
			'content': 'Cita Ortodoncia'
		},
		{
			'id': 2,
			'content': 'Cita Odontologia'
		},
		{
			'id': 3,
			'content': 'Cita Empresa'
		}
	])
)

agenda = Blueprint('agenda', __name__, template_folder='templates', static_folder='static')

@agenda.route('/', methods = [ 'GET' ])
def index():
	return render_template("index.html", agenda=dataAgenda)

@agenda.route('/create', methods = [ 'GET', 'POST' ])
def create():
	form = CreateForm()
	if form.validate_on_submit():
		id = form.id.data
		content = form.content.data

		dataAgenda.append({ 'id': id, 'content': content })

		return redirect(url_for('agenda.index'))
	return render_template("create.html", form=form)